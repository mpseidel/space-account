'use strict';
const space_config_1 = require("@ms3/space-config");
const util_1 = require("util");
const azure = require("azure-storage");
const _ = require("lodash");
let eg = azure.TableUtilities.entityGenerator;
var accountName = space_config_1.config.get("azure_table_storage_name");
var accountKey = space_config_1.config.get("azure_table_storage_access_key");
let tablePrefix = space_config_1.config.get('tablePrefix');
let appPartitionKey = space_config_1.config.get('appPartitionKey');
let usersTable = tablePrefix + 'Users';
var tableService = azure.createTableService(accountName, accountKey);
util_1.log(appPartitionKey);
function createUsersTable() {
    tableService.createTableIfNotExists(usersTable, (error, result, response) => {
        if (!error) {
            console.error(error);
        }
    });
}
exports.createUsersTable = createUsersTable;
function saveUser(user, id, hash) {
    console.log('saveUser');
    let userToStore = generateEntity(user);
    userToStore.PartitionKey = eg.String(appPartitionKey);
    userToStore.RowKey = eg.String(id);
    userToStore.Id = eg.String(id);
    userToStore.Password = eg.String(hash);
    console.log('entity generated', userToStore);
    return new Promise((resolve, reject) => {
        tableService.insertEntity(usersTable, userToStore, (err, result) => {
            if (err) {
                reject(err);
                console.error(err);
            }
            console.log('resolved!', result);
            resolve(result.RowKey);
        });
    });
}
exports.saveUser = saveUser;
function generateEntity(obj) {
    var entity = _.clone(obj);
    for (var property in entity) {
        if (property !== '_metadata') {
            if (_.isArray(entity[property])) {
                entity[property] = JSON.stringify(entity[property]);
                continue;
            }
            if (_.isBoolean(entity[property])) {
                entity[property] = eg.Boolean(entity[property]);
                continue;
            }
            if (_.isDate(entity[property])) {
                entity[property] = eg.DateTime(entity[property]);
                continue;
            }
            if (_.isString(entity[property])) {
                entity[property] = eg.String(entity[property]);
                continue;
            }
            if (_.isObject(entity[property])) {
                entity[property] = eg.String(JSON.stringify(entity[property]));
                continue;
            }
            entity[property] = eg.Entity(entity[property]);
        }
    }
    return entity;
}
function getUserByEmailAsync(email) {
    return new Promise((resolve, reject) => {
        tableService.retrieveEntity(usersTable, appPartitionKey, email, (error, result, response) => {
            if (!error) {
                // result contains the entity
                console.log(result);
                resolve(result);
            }
            else {
                reject(error);
            }
        });
    });
}
exports.getUserByEmailAsync = getUserByEmailAsync;
//# sourceMappingURL=userdb.js.map