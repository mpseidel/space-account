import { RequestHandler } from "express";
export declare function validate(protect: boolean): RequestHandler;
export declare function login(req: any, res: any): any;
export declare function logout(req: any, res: any): any;
export declare function refresh(req: any, res: any): any;
