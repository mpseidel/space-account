"use strict";
const bcrypt = require("bcrypt-nodejs");
const SALT_WORK_FACTOR = 10;
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = {
    hash: (clearTextPassword) => {
        return new Promise((resolve, reject) => {
            bcrypt.genSalt(SALT_WORK_FACTOR, function (err, salt) {
                if (err) {
                    return reject(err);
                }
                bcrypt.hash(clearTextPassword, salt, null, function (err, hash) {
                    if (err) {
                        return reject(err);
                    }
                    console.log(hash);
                    resolve(hash);
                });
            });
        });
    },
    compare: (clearTextPassword, hash) => {
        return new Promise((resolve, reject) => {
            bcrypt.compare(clearTextPassword, hash, function (err, isMatch) {
                if (err) {
                    return reject(err);
                }
                console.log('do they match?', isMatch);
                resolve(isMatch);
            });
        });
    }
};
//# sourceMappingURL=hasher.js.map