import bcrypt = require("bcrypt-nodejs");
const SALT_WORK_FACTOR = 10;

export default {
  hash: (clearTextPassword) => {
    return new Promise<string>((resolve, reject) => {
      bcrypt.genSalt(SALT_WORK_FACTOR, function (err, salt) {
        if (err) {
          return reject(err);
        }

        bcrypt.hash(clearTextPassword, salt, null, function (err, hash) {
          if (err) {
            return reject(err);
          }

          console.log(hash);

          resolve(hash);
        });
      });
    });
  },
  compare: (clearTextPassword, hash) => {
    return new Promise<boolean>((resolve, reject) => {

      bcrypt.compare(clearTextPassword, hash, function (err, isMatch) {
        if (err) {
          return reject(err);
        }

        console.log('do they match?', isMatch);
        resolve(isMatch);
      });
    });
  }
}
