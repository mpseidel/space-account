import {hash} from "bcrypt-nodejs";
import {RequestHandler} from "express";
import hasher from './hasher';
import * as userdb from './userdb';
import jwt = require('jsonwebtoken');
import Cookies = require('cookies');
import moment = require('moment');
let createNamespace = require('continuation-local-storage').createNamespace;
let ns = createNamespace('skytalk');
let uuid = require('node-uuid').v4;


userdb.createUsersTable();

export async function saveUser(user, password):Promise<string> {
  let hash = await hasher.hash(password);
  let id = uuid();
  console.log("user id generated", id);
  let promise = userdb.saveUser(user, id, hash);
  console.log("createUserPromise created", promise);
  return promise;
}

export function validate(protect:boolean):RequestHandler {
  let handler = function (req, res, next) {

    try {
      // check header or url parameters or post parameters for token
      // var token = req.body.token || req.query.token || req.headers['x-access-token'];

      req.authenticated = false;

      let token = new Cookies(req, res).get('access_token');

      // decode token
      if (token) {

        // verifies secret and checks exp
        jwt.verify(token, req.app.get('superSecret'), (err, decoded) => {
          if (err) {
            if (protect)
              return res.json({success: false, message: 'Failed to authenticate token.'});
            else
              next();

          } else {

            ns.bindEmitter(req);
            ns.bindEmitter(res);
            ns.run(function () {
              ns.set('account-id', decoded.accountId);
              // if everything is good, save to request for use in other routes
              req.decoded = decoded;
              req.authenticated = true;
              req.userId = decoded.id;

              next();
            });
          }
        });

      } else if (protect) {

        // if there is no token
        // return an error
        return res.status(403).send({
          success: false,
          message: 'No token provided.'
        });

      } else {
        next();
      }
    } catch (e) {
      res.status(500).json({error: e.message});
    }
  };

  return handler;
}

function setCookie(user, req:any, res:any):void {

// if user is found and password is right
  // create a token
  let token = jwt.sign(user, req.app.get('superSecret'), {
    expiresInMinutes: 1440 // expires in 24 hours
  });

  let cookies = new Cookies(req, res);
  // set a regular cookie
  cookies.set('access_token', token, {
    httpOnly: true,
    expires: moment().add(30, 'minute').toDate()
    // secure: true      // for your production environment
  });
}

export async function login(req, res) {
  try {
    let username = req.body.username;
    let password = req.body.password;

    console.log('logging in user ' + username);

    let hash = await hasher.hash(password);

    // await saveUser({email: 'mpseidel@outlook.com'}, 'foobar');
    // console.log('user saved');

    let user = userdb.getUserByEmailAsync(username);

    user.then(async (result) => {
        let entity:any = result;
        if (!entity) {
          res.sendStatus(401);
        }

        let isPasswordCorrect = await hasher.compare(password, entity.Password._);
        if (!isPasswordCorrect) {
          res.sendStatus(401);
        }

        let userObj = {
          id: entity.Id._,
          accountId: entity.AccountId._,
          name: username,
        };
        console.log(entity);

        console.log(userObj);

        setCookie(userObj, req, res);

        // return the information including token as JSON
        res.json({
          name: userObj.name,
          id: userObj.id,
          success: true,
          message: 'Enjoy your token-cookie!'
        });

      },
      rejection => {
        console.log('promise.rejection');
        console.log(rejection);
        res.status(500).send({
          error: 'user not found'
        });
      }).catch(e => {
      console.log(e);
      res.status(500).send({
        error: e
      });
    });

  } catch (e) {
    res.status(500).json({error: e.message});
  }
}

export function logout(req, res):any {
  res.clearCookie('access_token');
  req.authenticated = false;
  res.redirect('/#/login');
}

export function refresh(req, res):any {

  let token = new Cookies(req, res).get('access_token');
  let secret = req.app.get('superSecret');

  // verifies secret and checks exp
  jwt.verify(token, secret, function (err, decoded):any {
      if (err)
        res.json({success: false, message: 'Failed to authenticate token.'});

      setCookie(decoded, req, res);
      res.json({success: true});
      return;
    }
  );
}

