'use strict';

import {config} from '@ms3/space-config';
import {log} from "util";
import azure = require('azure-storage');
import _ = require('lodash');

let eg = azure.TableUtilities.entityGenerator;
var accountName = config.get("azure_table_storage_name");
var accountKey = config.get("azure_table_storage_access_key");
let tablePrefix = config.get('tablePrefix');
let appPartitionKey = config.get('appPartitionKey');
let usersTable = tablePrefix + 'Users';

var tableService = azure.createTableService(accountName, accountKey);

log(appPartitionKey);

export function createUsersTable():any {

  tableService.createTableIfNotExists(usersTable, (error, result, response):any => {
    if (!error) {
      console.error(error);
    }
  });
}

export function saveUser(user, id, hash):Promise<string> {
  console.log('saveUser');
  let userToStore = generateEntity(user);

  userToStore.PartitionKey = eg.String(appPartitionKey);
  userToStore.RowKey = eg.String(id);
  userToStore.Id = eg.String(id);
  userToStore.Password = eg.String(hash);

  console.log('entity generated', userToStore);
  return new Promise<string>((resolve, reject) => {
    tableService.insertEntity(usersTable, userToStore, (err,result) => {
      if(err){
        reject(err);
        console.error(err);
      }

      console.log('resolved!', result);
      resolve(result.RowKey);

    });
  });
}

function generateEntity(obj) {
  var entity = _.clone(obj);
  for (var property in entity) {
    if (property !== '_metadata') {
      if (_.isArray(entity[property])) {
        entity[property] = JSON.stringify(entity[property]);
        continue;
      }
      if (_.isBoolean(entity[property])) {
        entity[property] = eg.Boolean(entity[property]);
        continue;
      }
      if (_.isDate(entity[property])) {
        entity[property] = eg.DateTime(entity[property]);
        continue;
      }
      if (_.isString(entity[property])) {
        entity[property] = eg.String(entity[property]);
        continue;
      }
      if (_.isObject(entity[property])) {
        entity[property] = eg.String(JSON.stringify(entity[property]));
        continue;
      }

      entity[property] = eg.Entity(entity[property]);
    }
  }
  return entity;
}

export function getUserByEmailAsync(email):Promise<any> {
  return new Promise((resolve, reject) => {
    tableService.retrieveEntity(usersTable, appPartitionKey, email, (error, result, response):any => {
      if (!error) {
        // result contains the entity
        console.log(result);
        resolve(result);
      }
      else {
        reject(error);
      }
    });
  });
}

